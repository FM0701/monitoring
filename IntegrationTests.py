import ExceedanceCheckModule as ECM
import MailerService as MS
import unittest
from unittest import mock

class TestRuntimeModule(unittest.TestCase):
    
    @mock.patch.object(ECM, 'log')
    def test_check_for_excess_info(self, mock_log):
        ECM.check_for_excess(80, 90, 50, 'CPU')
        mock_log.assert_called_with('info', 'CPU', 90, 80, 50)

    @mock.patch.object(ECM, 'log')
    def test_check_for_excess_warning(self, mock_log):
        ECM.check_for_excess(80, 90, 85, 'RAM')
        mock_log.assert_called_with('warning', 'RAM', 90, 80, 85)
        
    @mock.patch.object(ECM, 'log')
    def test_check_for_excess_critical(self, mock_log):
        ECM.check_for_excess(80, 90, 95, 'Disk C:')
        mock_log.assert_called_with('critical', 'Disk C:', 90, 80, 95)

    @mock.patch.object(MS, 'send_email')
    def test_send_mail(self, mock_send_email):
        ECM.check_for_excess(80, 90, 95, 'Disk C:')
        mock_send_email.assert_called()   
   
    @mock.patch.object(MS, 'send_email')     
    def test_get_message_for_mail(self, mock_send_email):
        ECM.check_for_excess(80, 90, 95, 'Disk C:')
        mock_send_email.assert_called_with('Subject: CRITICAL INCIDENT\n\nThe following object has exceeded the hardlimit: Disk C:\n Limit: 90\n Current Value: 95\n Please perform an action!')

if __name__ == '__main__':
    unittest.main(verbosity=2)