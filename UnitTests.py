import ExceedanceCheckModule as ECM
import platform as pl
import unittest
import time

class TestRuntimeModule(unittest.TestCase):  

    def test_get_message_for_log(self):
        log_message = ECM.get_message_for_log('CPU', 'soft limit', 50, 55)
        log_assert = str(pl.uname().node) + ' - CPU --- soft limit: 50% --- current value: 55%'
        self.assertEquals(log_message, log_assert)
        
    def test_get_message_for_mail(self):
        mail_message = ECM.get_message_for_mail('RAM', 70, 80)
        mail_assert = 'Subject: CRITICAL INCIDENT\n\nThe following object has exceeded the hardlimit: RAM\n Limit: 70\n Current Value: 80\n Please perform an action!'
        self.assertEquals(mail_message, mail_assert)
        
    def test_check_last_executed_false(self): 
        last_time = time.time()
        time.sleep(1)
        self.assertFalse(ECM.check_last_executed(last_time, 5))
        
    def test_check_last_executed_true(self): 
        last_time = time.time()
        time.sleep(2)
        self.assertTrue(ECM.check_last_executed(last_time, 1))

if __name__ == '__main__':
    unittest.main(verbosity=2)
