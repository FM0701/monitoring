import MonitoringModule as MM
import time as tm

while True:
    MM.get_data()
    MM.check_value(80, 90, MM.cpu)
    MM.check_value(80, 90, MM.ram)
    MM.check_value(80, 90, MM.disk_storage)
    tm.sleep(10)
